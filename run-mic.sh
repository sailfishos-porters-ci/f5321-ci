#!/bin/sh
sudo zypper -n in lvm2 atruncate pigz

sudo ssu ar unbreakmic http://repo.merproject.org/obs/home:/sledge:/branches:/mer-tools:/devel/latest_i486/
sudo zypper ref unbreakmic
sudo zypper rm droid-tools # it's ok if you hadn't any
sudo zypper -n in android-tools

sudo mknod /dev/loop0 -m0660 b 7 0
sudo mknod /dev/loop1 -m0660 b 7 1
sudo mknod /dev/loop2 -m0660 b 7 2

sudo mic create loop --arch=$PORT_ARCH \
--tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,EXTRA_NAME:$EXTRA_NAME \
--record-pkgs=name,url \
--outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
Jolla-@RELEASE@-$DEVICE-@ARCH@.ks


