# Sailfish OS on Xperia X Compact (f5321)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build
2.2.1.18 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/f5321-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/5321-ci/commits/master)



